import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedGuard } from './modules/auth/guards/authenticated.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
    data: { title: 'Authorization', htmlTitle: 'Authorization' }
  },
  {
    path: '',
    loadChildren: () => import('./modules/authenticated-area/authenticated-area.module').then(m => m.AuthenticatedAreaModule),
    canActivate: [ AuthenticatedGuard ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
