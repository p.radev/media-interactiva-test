import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionService } from '../../../core/services/session.service';
import { Router } from '@angular/router';
import { AccessService } from '../../../core/services/access.service';
import { NgForm } from '@angular/forms';
import { BaseSafeSubscriber } from '../../../shared/components-base/base-safe-subscriber';
import { FormsUtils } from '../../../shared/utils/forms-utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent extends BaseSafeSubscriber implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;

  public login = 'main@mailinator.com';
  public password = 'Main1718';

  error: string;

  public isLoading: boolean;
  public isPasswordVisible = false;

  constructor(private sessionService: SessionService,
              private accessService: AccessService,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.registerSubscription(this.sessionService.CurrentUserObservable
      .subscribe((user) => {
        if (user) {
          this.router.navigate(['/']).then();
        }
      }));
  }

  async signin(): Promise<void> {
    this.isLoading = true;

    try {
      await this.sessionService.SigninWithCredentials(this.login, this.password);
      this.error = null;
      this.isLoading = false;
    } catch (e) {
      this.error = e?.message || null;

      FormsUtils.TryToApplyBackendErrorsToForm(this.loginForm, e);
    } finally {
      this.isLoading = false;
    }
  }
}
