import { ObjectWithTimestampsFilterModel } from './object-with-timestamps-filter.model';

export class PetsFilterModel extends ObjectWithTimestampsFilterModel {
  public ownerId: string;
}
