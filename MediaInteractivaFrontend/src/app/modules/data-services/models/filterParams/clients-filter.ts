﻿import { ObjectWithTimestampsFilterModel } from './object-with-timestamps-filter.model';

export class ClientsFilter extends ObjectWithTimestampsFilterModel {
  public name: string;
}
