﻿import { SoftDeletableFilterModel } from './soft-deletable-filter.model';

export class ObjectWithTimestampsFilterModel extends SoftDeletableFilterModel {
  public createdBy: string[];
  public createdFrom: Date;
  public createdTo: Date;

  public updatedBy: string[];
  public updatedFrom: Date;
  public updatedTo: Date;
}
