﻿export class PetUpdateModel {
  public name: string;
  public typeId: string;
  public ownerId: string;
}
