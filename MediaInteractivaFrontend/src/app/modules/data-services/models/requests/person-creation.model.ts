﻿export class PersonCreationModel {
  public firstName: string;
  public lastName: string;
  public isEmployee: boolean;
}
