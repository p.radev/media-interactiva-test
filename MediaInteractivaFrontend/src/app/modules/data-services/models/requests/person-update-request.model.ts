﻿export class PersonUpdateRequestModel {
  public firstName: string;
  public lastName: string;
  public isEmployee: boolean;
}
