﻿export class PetCreationModel {
  public name: string;
  public typeId: string;
  public ownerId: string;
}
