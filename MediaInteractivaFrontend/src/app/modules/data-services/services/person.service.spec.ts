import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { PersonService } from './person.service';
import { SessionService } from '../../core/services/session.service';
import { AppSettings } from '../../core/AppSettings';
import { LocalStorageExtendedService } from '../../core/services/local-storage-extended.service';
import { ApiClientService } from '../../core/services/api-client.service';
import { Person } from '../../core/models/person';
import { ListWithTotals } from '../models/ListWithTotals';
import { ApiResponse } from '../../core/models/apiResponse.model';
import { PersonCreationModel } from '../models/requests/person-creation.model';

describe('PersonService', () => {
  let injector: TestBed;
  let service: PersonService;
  let httpMock: HttpTestingController;
  let apiClientService: ApiClientService;

  const listOfPerson: Person[] = [
    {
      firstName: 'test1',
      lastName: 'test1',
      isEmployee: true,
      id: "1"
    }, {
      firstName: 'test2',
      lastName: 'test2',
      isEmployee: true,
      id: "2"
    }, {
      firstName: 'test3',
      lastName: 'test3',
      isEmployee: true,
      id: "3"
    },
  ];


  const listWithTotals: ListWithTotals<Person> = {
    isEmpty: false,
    totalCount: 3,
    currentPage: 0,
    pageSize: 5,
    skippedItems: 0,
    list: listOfPerson
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PersonService,
        ApiClientService,
        AppSettings,
        SessionService,
        LocalStorageExtendedService
      ],
    });

    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
    apiClientService = injector.inject(ApiClientService);

    service = new PersonService(apiClientService);
  });

  afterEach(() => {
    httpMock.verify();
  });

  describe('#getList', () => {
    it('should return data', async () => {
      const methodPromise = service.getList().toPromise();

      const req = httpMock.expectOne(AppSettings.getAPIUrl('persons'));
      expect(req.request.method).toBe('GET');

      const resp: ApiResponse<ListWithTotals<Person>> = {
        data: listWithTotals,
        status: "success",
        errorDetails: null
      };

      req.flush(resp);

      const res = await methodPromise;

      expect(res).toEqual(listWithTotals);
    });
  });

  describe('#createPerson', () => {
    it('should send creation request and return person', async () => {
      const model: PersonCreationModel = {
        firstName: "Test",
        lastName: "Test",
        isEmployee: true
      };

      const methodPromise = service.createPerson(model).toPromise();

      const req = httpMock.expectOne(AppSettings.getAPIUrl('persons/create'));
      expect(req.request.method).toBe('POST');

      const newPerson: Person = {
        id: "someId",
        firstName: model.firstName,
        lastName: model.lastName,
        isEmployee: model.isEmployee,
      };

      const resp: ApiResponse<Person> = {
        data: newPerson,
        status: "success",
        errorDetails: null
      };

      req.flush(resp);

      const res = await methodPromise;

      expect(res.firstName).toBe(model.firstName);
      expect(res.lastName).toBe(model.lastName);
      expect(res.isEmployee).toBe(model.isEmployee);
    });
  });
});
