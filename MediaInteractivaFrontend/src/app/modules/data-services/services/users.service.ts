﻿import { Observable } from 'rxjs';
import { ApiClientService } from '../../core/services/api-client.service';
import { ListWithTotals } from '../models/ListWithTotals';
import { ClientsFilter } from '../models/filterParams/clients-filter';
import { Injectable } from '@angular/core';
import { User } from '../../core/models/user';

@Injectable()
export class UsersService {
  constructor(private apiClient: ApiClientService) {
  }

  getAllUsersList = (getParams: Partial<ClientsFilter> = {}): Observable<ListWithTotals<User>> =>
    this.apiClient.get<ListWithTotals<User>>('users', getParams)

  getById = (id: string): Observable<User> =>
    this.apiClient.get<User>(`users/${id}`)
}
