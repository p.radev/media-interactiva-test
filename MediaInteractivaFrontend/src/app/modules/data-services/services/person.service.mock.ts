import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ListWithTotals } from '../models/ListWithTotals';
import { BaseListFilterModel } from '../models/filterParams/base-list-filter-model';
import { Person } from '../../core/models/person';

@Injectable()
export class PersonServiceMock {

  public getList(getParams: Partial<BaseListFilterModel> = {}):  Observable<ListWithTotals<Person>> {

    var listOfPerson : Person[] = [
      {
        firstName: 'test1',
        lastName: 'test1',
        isEmployee: true,
        id: "1"
      },{
        firstName: 'test2',
        lastName: 'test2',
        isEmployee: true,
        id: "2"
      },{
        firstName: 'test3',
        lastName: 'test3',
        isEmployee: true,
        id: "3"
      },
    ];

    var listWithTotals = new ListWithTotals<Person>();
    listWithTotals.totalCount = 3;
    listWithTotals.currentPage = 0;
    listWithTotals.pageSize = 5;
    listWithTotals.skippedItems = 0;
    listWithTotals.list = listOfPerson;

    return of(listWithTotals)
  }

}
