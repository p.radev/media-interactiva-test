﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiClientService } from '../../core/services/api-client.service';
import { ListWithTotals } from '../models/ListWithTotals';
import { BaseListFilterModel } from '../models/filterParams/base-list-filter-model';
import { Person } from '../../core/models/person';
import { ApiResponse } from '../../core/models/apiResponse.model';
import { PersonCreationModel } from '../models/requests/person-creation.model';
import { PersonUpdateRequestModel } from '../models/requests/person-update-request.model';

@Injectable()
export class PersonService {
  constructor(private apiClient: ApiClientService) {
  }

  getPersonById = (id: string): Observable<Person> =>
    this.apiClient.get<Person>(`persons/${id}`)

  createPerson = (data: PersonCreationModel): Observable<Person> =>
    this.apiClient.post<Person>('persons/create', data)

  updatePerson = (id: string, model: PersonUpdateRequestModel): Observable<Person> =>
    this.apiClient.put<Person>(`persons/${id}`, model)

  getList = (getParams: Partial<BaseListFilterModel> = {}): Observable<ListWithTotals<Person>> =>
    this.apiClient.get<ListWithTotals<Person>>('persons', getParams)

  getAllList = (): Observable<Array<Person>> =>
    this.apiClient.get<Array<Person>>('persons/all')

  deactivatePerson = (id: string): Observable<ApiResponse<boolean>> =>
    this.apiClient.post<ApiResponse<boolean>>(`persons/deactivate/${id}`)

  fullyRemoveClient = (id: string): Observable<ApiResponse<boolean>> =>
    this.apiClient.post<ApiResponse<boolean>>(`persons/fully-remove/${id}`)

  restoreClient = (id: string): Observable<ApiResponse<Person>> =>
    this.apiClient.post<ApiResponse<Person>>(`persons/restore/${id}`)
}
