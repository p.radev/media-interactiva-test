﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ListWithTotals } from '../models/ListWithTotals';
import { ApiClientService } from '../../core/services/api-client.service';
import { Pet } from '../../core/models/pet';
import { PetCreationModel } from '../models/requests/pet-creation.model';
import { PetUpdateModel } from '../models/requests/pet-update-request.model';
import { AnymalType } from '../../core/models/AnymalType';
import { PetsFilterModel } from '../models/filterParams/pets-filter.model';

@Injectable()
export class PetsService {
  constructor(private apiClient: ApiClientService) {
  }

  getPetById = (id: string): Observable<Pet> =>
    this.apiClient.get<Pet>(`pets/${id}`)

  createPet = (data: PetCreationModel): Observable<PetCreationModel> =>
    this.apiClient.post<PetCreationModel>('pets/create', data)


  updatePet = (id: string, model: PetUpdateModel): Observable<Pet> =>
    this.apiClient.put<Pet>(`pets/${id}`, model)


  getList = (getParams: Partial<PetsFilterModel> = {}): Observable<ListWithTotals<Pet>> =>
    this.apiClient.get<ListWithTotals<Pet>>('pets', getParams)

  getTypes = (): Observable<Array<AnymalType>> =>
    this.apiClient.get<Array<AnymalType>>('pets/types')

  remove = (id: string): Observable<Array<AnymalType>> =>
    this.apiClient.get<Array<AnymalType>>('pets/fully-remove/' + id)
}
