import { Component, OnInit } from '@angular/core';
import { ListWithTotals } from '../../../data-services/models/ListWithTotals';
import { Subject } from 'rxjs';
import { PetsService } from '../../../data-services/services/pets.service';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, takeWhile } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort/sort';
import isEqual from 'lodash-es/isEqual';
import { PetCreationDialogComponent } from '../../dialog-components/pet-creation-dialog/pet-creation-dialog.component';
import { Pet } from 'src/app/modules/core/models/pet';
import { TdDialogService } from '@covalent/core/dialogs';
import { PetsFilterModel } from '../../../data-services/models/filterParams/pets-filter.model';
import { ActivatedRoute } from '@angular/router';
import { PetEditDialogComponent } from '../../dialog-components/pet-edit-dialog/pet-edit-dialog.component';
import { Person } from 'src/app/modules/core/models/person';
import { PersonService } from 'src/app/modules/data-services/services/person.service';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html'
})
export class PetsComponent implements OnInit {
  public isAlive = true;

  public tableData: ListWithTotals<Pet>;
  public isLoading = true;

  public filters = new PetsFilterModel();
  private updateDataCalledSubject$ = new Subject();

  public persons: Array<Person>;

  public isAnyPetsExists = false;

  constructor(
    private petsService: PetsService,
    private toastr: ToastrService,
    private personsService: PersonService,
    private activatedRoute: ActivatedRoute,
    private dialogService: TdDialogService) {
  }

  ngOnInit(): void {
    this.updateDataCalledSubject$
      .pipe(
        debounceTime(300),
        takeWhile(() => this.isAlive)
      )
      .subscribe(async () => {
        await this.updateData();
      });

    this.callDataUpdate();

    this.activatedRoute.queryParams.subscribe((params) => {
      if (params.ownerId && params.ownerId != this.filters.ownerId) {
        this.filters.ownerId = params.ownerId;
        this.callDataUpdate();
      }
    })
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  public acceptFilter(): void {
    console.log('e', "acceptFilters");
    this.callDataUpdate();
  }

  private async updateData(): Promise<void> {
    try {
      this.isLoading = true;
      this.tableData = await this.petsService.getList(this.filters).toPromise();
      this.persons = await this.personsService.getAllList().toPromise();

      if (!this.isAnyPetsExists && this.tableData.totalCount) {
        this.isAnyPetsExists = true;
      }
    } catch (e) {
      console.log('e', e);
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

  private callDataUpdate(): void {
    this.updateDataCalledSubject$.next();
  }

  onPaginationChanged(event: PageEvent): void {
    this.filters.pageSize = event.pageSize;
    this.filters.page = event.pageIndex;
    this.callDataUpdate();
  }

  onSort(event: Sort): void {
    this.filters.orderedBy = event.active;

    this.filters.orderReversed = isEqual(event.direction, 'asc');
    this.callDataUpdate();
  }

  callPetCreationDialog(): void {
    const dialogRef = this.dialogService.open(PetCreationDialogComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.callDataUpdate();
      }
    });
  }

  callPetEditDialog(petId: string): void {
    const dialogRef = this.dialogService.open(PetEditDialogComponent, { data: { petId } });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.callDataUpdate();
      }
    });
  }

  callRemovalDialog(petId: string): void {
    const dialogRef = this.dialogService.openConfirm({ message: 'Pet will be fully removed' });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res) {
        await this.petsService.remove(petId).toPromise();
        this.callDataUpdate();
      }
    });
  }

}
