import { Component, OnInit } from '@angular/core';
import { combineLatest } from 'rxjs';
import { SessionService } from '../../../core/services/session.service';
import { Router } from '@angular/router';
import { BaseSafeSubscriber } from '../../../shared/components-base/base-safe-subscriber';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html'
})
export class MainLayoutComponent extends BaseSafeSubscriber implements OnInit {

  constructor(private sessionService: SessionService,
              private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.registerSubscription(
      combineLatest([
        this.sessionService.CurrentUserObservable,
      ]).subscribe(([currentUser]) => {
        if (!currentUser) {
          this.navigateToLoginPage();
        }
      }));
  }

  private navigateToLoginPage(): void {
    this.router.navigate(['/auth/login']).then();
  }
}
