import { Component, OnDestroy, OnInit } from '@angular/core';
import { ListWithTotals } from '../../../data-services/models/ListWithTotals';
import { Subject } from 'rxjs';
import { PersonService } from '../../../data-services/services/person.service';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, takeWhile } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort/sort';
import isEqual from 'lodash-es/isEqual';
import { Person } from '../../../core/models/person';
import { ObjectWithTimestampsFilterModel } from '../../../data-services/models/filterParams/object-with-timestamps-filter.model';
import { PersonCreationDialogComponent } from '../../dialog-components/person-creation-dialog/person-creation-dialog.component';
import { Router } from '@angular/router';
import { TdDialogService } from '@covalent/core/dialogs';
import { PersonEditDialogComponent } from '../../dialog-components/person-edit-dialog/person-edit-dialog.component';

@Component({
  selector: 'app-emploees',
  templateUrl: './employees.component.html'
})
export class EmployeesComponent implements OnInit, OnDestroy {
  public isAlive = true;

  public tableData: ListWithTotals<Person>;
  public isLoading = true;

  public filters = new ObjectWithTimestampsFilterModel();
  private updateDataCalledSubject$ = new Subject();


  public isAnyClientsExists = false;

  constructor(
    private personsService: PersonService,
    private toastr: ToastrService,
    private dialogService: TdDialogService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.updateDataCalledSubject$
      .pipe(
        debounceTime(300),
        takeWhile(() => this.isAlive)
      )
      .subscribe(async () => {
        await this.updateData();
      });

    this.callDataUpdate();
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  public async updateData(): Promise<void> {
    try {
      this.isLoading = true;
      this.tableData = await this.personsService.getList(this.filters).toPromise();

      if (!this.isAnyClientsExists && this.tableData.totalCount) {
        this.isAnyClientsExists = true;
      }
    } catch (e) {
      console.log('e', e);
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

  public callDataUpdate(): void {
    this.updateDataCalledSubject$.next();
  }

  onPaginationChanged(event: PageEvent): void {
    this.filters.pageSize = event.pageSize;
    this.filters.page = event.pageIndex;
    this.callDataUpdate();
  }

  onSort(event: Sort): void {
    this.filters.orderedBy = event.active;

    this.filters.orderReversed = isEqual(event.direction, 'asc');
    this.callDataUpdate();
  }

  callPersonCreationDialog(): void {
    const dialogRef = this.dialogService.open(PersonCreationDialogComponent);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.callDataUpdate();
      }
    });
  }

  callPersonEditDialog(personId: string): void {
    const dialogRef = this.dialogService.open(PersonEditDialogComponent, { data: { personId } });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.callDataUpdate();
      }
    });
  }

  deactivateUser(id: string): void {
    const dialogRef = this.dialogService.openConfirm({ message: 'The person will be deactivated' });

    dialogRef.afterClosed().subscribe(async (res) => {
      if (res) {
        await this.personsService.deactivatePerson(id).toPromise();
        this.callDataUpdate();
      }
    });
  }

}
