import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TdDialogService } from '@covalent/core/dialogs';
import { ToastrModule } from 'ngx-toastr';
import { PersonService } from '../../../data-services/services/person.service';
import { PersonServiceMock } from '../../../data-services/services/person.service.mock';
import { EmployeesComponent } from './employees.component';
import { ApiClientService } from '../../../core/services/api-client.service';
import { SessionService } from '../../../core/services/session.service';
import { LocalStorageExtendedService } from '../../../core/services/local-storage-extended.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';

describe('EmployeesComponent', () => {
  let component: EmployeesComponent;
  let fixture: ComponentFixture<EmployeesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [EmployeesComponent],
      imports: [
        HttpClientTestingModule,
        ToastrModule.forRoot(),
        MatDialogModule,
        RouterTestingModule.withRoutes([{
          path: 'employees',
          component: EmployeesComponent
        }])
      ],
      providers: [

        { provide: PersonService, useClass: PersonServiceMock },
        ApiClientService,
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        SessionService,
        LocalStorageExtendedService,
        { provide: TdDialogService, useClass: TdDialogService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have initial values', () => {
    expect(component.isAlive).toBe(true);
    expect(component.isLoading).toBe(true);
    expect(component.filters).not.toBeNull();
  });

  describe('#updateData', () => {
    it('should fill tableData', async function () {
      await component.updateData();

      expect(component.tableData.list.length).toBeGreaterThan(0);
      expect(component.tableData.isEmpty).toBe(false);
      expect(component.isAnyClientsExists).toBe(true);
    });
  });

  describe('#onPaginationChanged', () => {
    it('should change filters state', () => {
      const event: PageEvent = {
        pageSize: 20,
        pageIndex: 0,
        length: 100
      };

      component.onPaginationChanged(event);

      expect(component.filters.pageSize).toBe(event.pageSize);
      expect(component.filters.page).toBe(event.pageIndex);
    });
  });
});
