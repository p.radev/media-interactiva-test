import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PersonService } from '../../../data-services/services/person.service';
import { ToastrService } from 'ngx-toastr';
import { PersonUpdateRequestModel } from '../../../data-services/models/requests/person-update-request.model';

export interface IPersonEditInputData {
  personId: string;
}

@Component({
  selector: 'app-person-edit-dialog',
  templateUrl: './person-edit-dialog.component.html'
})
export class PersonEditDialogComponent implements OnInit {
  public model = new PersonUpdateRequestModel();

  constructor(@Inject(MAT_DIALOG_DATA) public initialData: IPersonEditInputData,
              public dialogRef: MatDialogRef<PersonEditDialogComponent>,
              private personsService: PersonService,
              private toastr: ToastrService) {
  }

  public isLoading = false;

  async ngOnInit(): Promise<void> {
    await this.initializeModelToUpdate();
  }

  private async initializeModelToUpdate(): Promise<void> {
    try {
      this.isLoading = true;
      const person = await this.personsService.getPersonById(this.initialData.personId).toPromise();

      this.model = {
        firstName: person.firstName,
        isEmployee: person.isEmployee,
        lastName: person.lastName,
      }
    } catch (e) {
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

  public async saveData(): Promise<void> {
    try {
      this.isLoading = true;
      const result = await this.personsService.updatePerson(this.initialData.personId, this.model).toPromise();

      this.toastr.success('Saved');
      this.dialogRef.close(result);
    } catch (e) {
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }
}
