import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PersonCreationModel } from '../../../data-services/models/requests/person-creation.model';
import { PersonService } from '../../../data-services/services/person.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-person-creation-dialog',
  templateUrl: './person-creation-dialog.component.html'
})
export class PersonCreationDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public initialData: any,
              public dialogRef: MatDialogRef<PersonCreationDialogComponent>,
              private personsService: PersonService,
              private toastr: ToastrService) {
  }

  public model = new PersonCreationModel();
  public isLoading = false;

  ngOnInit(): void {
    this.model.isEmployee = true;
  }

  public async saveData(): Promise<void> {
    try {
      this.isLoading = true;
      const result = await this.personsService.createPerson(this.model).toPromise();

      this.toastr.success('Saved');
      this.dialogRef.close(result);
    } catch (e) {
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }
}
