import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PetsService } from '../../../data-services/services/pets.service';
import { ToastrService } from 'ngx-toastr';
import { PetCreationModel } from 'src/app/modules/data-services/models/requests/pet-creation.model';
import { AnymalType } from 'src/app/modules/core/models/AnymalType';
import { Subject } from 'rxjs';
import { debounceTime, takeWhile } from 'rxjs/operators';
import { Person } from 'src/app/modules/core/models/person';
import { PersonService } from 'src/app/modules/data-services/services/person.service';

@Component({
  selector: 'app-pet-creation-dialog',
  templateUrl: './pet-creation-dialog.component.html'
})
export class PetCreationDialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public initialData: any,
              public dialogRef: MatDialogRef<PetCreationDialogComponent>,
              private petsService: PetsService,
              private personsService: PersonService,
              private toastr: ToastrService) {
  }

  public isAlive = true;
  public model = new PetCreationModel();
  public isLoading = false;

  selectedType: string;
  selectedOwner: string;

  public types: Array<AnymalType>;
  public persons: Array<Person>;

  private updateDataCalledSubject$ = new Subject();

  ngOnInit(): void {
    this.updateDataCalledSubject$
      .pipe(
        debounceTime(300),
        takeWhile(() => this.isAlive)
      )
      .subscribe(async () => {
        await this.updateData();
      });

    this.callDataUpdate();
  }

  private async updateData(): Promise<void> {
    try {
      this.isLoading = true;
      this.types = await this.petsService.getTypes().toPromise();

      this.persons = await this.personsService.getAllList().toPromise();


    } catch (e) {
      console.log('e', e);
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

  private callDataUpdate(): void {
    this.updateDataCalledSubject$.next();
  }

  public async saveData(): Promise<void> {
    try {
      this.isLoading = true;
      this.model.typeId = this.selectedType;
      this.model.ownerId = this.selectedOwner;

      console.log('e', this.model);
      const result = await this.petsService.createPet(this.model).toPromise();

      this.toastr.success('Saved');
      this.dialogRef.close(result);
    } catch (e) {
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

}
