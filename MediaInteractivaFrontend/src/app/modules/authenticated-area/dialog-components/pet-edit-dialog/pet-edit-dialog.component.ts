import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { PetsService } from '../../../data-services/services/pets.service';
import { ToastrService } from 'ngx-toastr';
import { PetUpdateModel } from '../../../data-services/models/requests/pet-update-request.model';
import { AnymalType } from 'src/app/modules/core/models/AnymalType';
import { Person } from 'src/app/modules/core/models/person';
import { PersonService } from 'src/app/modules/data-services/services/person.service';
import { Subject } from 'rxjs';
import { debounceTime, takeWhile } from 'rxjs/operators';

export interface IPetEditInputData {
  petId: string;
}

@Component({
  selector: 'app-pet-edit-dialog',
  templateUrl: './pet-edit-dialog.component.html'
})
export class PetEditDialogComponent implements OnInit {
  public model = new PetUpdateModel();
  public types: Array<AnymalType>;
  public persons: Array<Person>;

  constructor(@Inject(MAT_DIALOG_DATA) public initialData: IPetEditInputData,
              public dialogRef: MatDialogRef<PetEditDialogComponent>,
              private petsService: PetsService,
              private personsService: PersonService,
              private toastr: ToastrService) {
  }

  public isLoading = false;

  private updateDataCalledSubject$ = new Subject();

  selectedType: string;
  selectedOwner: string;

  public isAlive = true;

  async ngOnInit(): Promise<void> {

    this.updateDataCalledSubject$
      .pipe(
        debounceTime(300),
        takeWhile(() => this.isAlive)
      )
      .subscribe(async () => {
        await this.updateData();
        await this.initializeModelToUpdate();
      });

    this.callDataUpdate();
  }

  private callDataUpdate(): void {
    this.updateDataCalledSubject$.next();
  }

  private async updateData(): Promise<void> {
    try {
      this.isLoading = true;
      this.types = await this.petsService.getTypes().toPromise();

      this.persons = await this.personsService.getAllList().toPromise();

    } catch (e) {
      console.log('e', e);
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

  private async initializeModelToUpdate(): Promise<void> {
    try {
      this.isLoading = true;
      const pet = await this.petsService.getPetById(this.initialData.petId).toPromise();

      this.model.name = pet.name;
      this.model.typeId = pet.type.id;
      this.selectedType = this.model.typeId

      this.model.ownerId = pet.owner.id;
      this.selectedOwner = this.model.ownerId

      console.log('e', pet);
      console.log('e', this.model);
    } catch (e) {
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }

  public async saveData(): Promise<void> {
    try {
      this.isLoading = true;
      const result = await this.petsService.updatePet(this.initialData.petId, this.model).toPromise();

      this.toastr.success('Saved');
      this.dialogRef.close(result);
    } catch (e) {
      this.toastr.error(e.message || e);
    } finally {
      this.isLoading = false;
    }
  }
}
