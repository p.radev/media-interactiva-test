import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CovalentDialogsModule } from '@covalent/core/dialogs';
import { CovalentTabSelectModule } from '@covalent/core/tab-select';
import { SharedModule } from '../shared/shared.module';
import { AuthenticatedAreaRoutingModule } from './authenticated-area.routing';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PersonCreationDialogComponent } from './dialog-components/person-creation-dialog/person-creation-dialog.component';

import { PetCreationDialogComponent } from './dialog-components/pet-creation-dialog/pet-creation-dialog.component';
import { PetEditDialogComponent } from './dialog-components/pet-edit-dialog/pet-edit-dialog.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { PetsComponent } from './components/pets/pets.component';
import { AppHeaderComponent } from '../shared/components/app.header/app.header.component';
import { PersonEditDialogComponent } from './dialog-components/person-edit-dialog/person-edit-dialog.component';

@NgModule({
  declarations: [
    MainLayoutComponent,
    NotFoundComponent,
    PersonCreationDialogComponent,
    PersonEditDialogComponent,
    EmployeesComponent,
    PetsComponent,
    PetCreationDialogComponent,
    AppHeaderComponent,
    PersonEditDialogComponent,
    PetEditDialogComponent
  ],
  imports: [
    CommonModule,
    AuthenticatedAreaRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatTabsModule,

    CovalentTabSelectModule,
    MatTooltipModule,
    MatTableModule,
    SharedModule,
    MatSortModule,
    MatPaginatorModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    FormsModule,
    MatDividerModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    MatSlideToggleModule,
    CovalentDialogsModule,
    MatSidenavModule,
    MatDialogModule
  ],
  providers: []
})
export class AuthenticatedAreaModule {
}
