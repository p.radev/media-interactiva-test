import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingAnimationComponent } from './components/loading-animation/loading-animation.component';
import { BaseAsyncValidatorDirective } from './directives/base-async-validator.directive';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppHeaderComponent } from './components/app.header/app.header.component';


@NgModule({
  declarations: [
    LoadingAnimationComponent,
    BaseAsyncValidatorDirective
  ],
  exports: [
    LoadingAnimationComponent,
    BaseAsyncValidatorDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatCardModule,
    RouterModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule
  ]
})
export class SharedModule {
}
