﻿import { NgForm } from '@angular/forms';
import { ApiErrorDetailsModel } from '../../core/models/apiErrorDetails.model';
import forEach from 'lodash-es/forEach';
import isFunction from 'lodash-es/isFunction';

export class FormsUtils {
  public static TryToApplyBackendErrorsToForm(form: NgForm, errorData: ApiErrorDetailsModel): void {
    forEach(errorData?.fieldSpecificMessages, (v, k) => {
      const controlToApplyChangesTo = form.controls[k];

      if (controlToApplyChangesTo && isFunction(controlToApplyChangesTo.setErrors)) {
        controlToApplyChangesTo.setErrors({ serverError: v });
      }
    });
  }
}
