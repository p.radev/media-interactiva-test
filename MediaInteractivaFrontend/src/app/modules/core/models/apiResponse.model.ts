﻿import { ApiErrorDetailsModel } from './apiErrorDetails.model';

export class ApiResponse<T> {
  public status: string;
  public data: T;
  public errorDetails: ApiErrorDetailsModel;
}
