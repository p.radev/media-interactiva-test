﻿import { IdentifiableModel } from './identifiable.model';

export class AnymalType extends IdentifiableModel {
  public id: string;
  public name: string;
}
