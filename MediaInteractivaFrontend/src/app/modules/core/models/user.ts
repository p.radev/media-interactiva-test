﻿import { SoftDeletableModel } from './softDeletable.model';

export class User extends SoftDeletableModel {
  public email: string;
  public firstName: string;
  public lastName: string;
  public fullName: string;
  public isGlobalAdmin: boolean;
}
