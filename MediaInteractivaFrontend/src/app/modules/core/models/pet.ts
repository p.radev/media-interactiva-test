﻿import { AnymalType } from './AnymalType';
import { IdentifiableModel } from './identifiable.model';
import { Person } from './person';

export class Pet extends IdentifiableModel {
  public name: string;
  public type: AnymalType;

  public owner: Person;
}
