﻿import { IIdentifiable } from '../interfaces/iIdentifiable';

export class IdentifiableModel implements IIdentifiable {
  id: string;
}
