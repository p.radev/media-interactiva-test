﻿import { IObjectWithTimestamps } from '../interfaces/iObjectWithTimestamps';
import { User } from './user';
import { SoftDeletableModel } from './softDeletable.model';

export class ObjectWithTimestampsModel extends SoftDeletableModel implements IObjectWithTimestamps {
  public createdAt: Date;
  public createdByUserId: string;
  public createdByUser: User;

  public updateddAt: Date;
  public updatedByUserId: string;
  public updatedByUser: User;
}
