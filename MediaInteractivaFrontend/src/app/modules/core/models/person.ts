﻿import { IdentifiableModel } from './identifiable.model';

export class Person extends IdentifiableModel {
  public firstName: string;
  public lastName: string;

  public isEmployee: boolean;
}
