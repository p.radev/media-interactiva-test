﻿import { IdentifiableModel } from './identifiable.model';
import { ISoftDeletable } from '../interfaces/iSoftDeletable';
import { User } from './user';

export class SoftDeletableModel extends IdentifiableModel implements ISoftDeletable {
  public deletedAt: Date;
  public deletedByUser: User;
  public deletedByUserId: string;
}
