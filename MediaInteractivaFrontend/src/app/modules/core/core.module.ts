import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocalStorageExtendedService } from './services/local-storage-extended.service';
import { SessionService } from './services/session.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AuthInteceptor } from './interceptors/AuthTokenInterceptor';
import { AccessService } from './services/access.service';
import { ApiClientService } from './services/api-client.service';
import { SharedModule } from '../shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    MatInputModule,
    FormsModule
  ]
})
export class CoreModule {
  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        LocalStorageExtendedService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInteceptor,
          multi: true,
        },
        SessionService,
        ApiClientService,
        AccessService,
      ]
    };
  }
}
