﻿import { User } from '../models/user';

export interface ISoftDeletable {
  deletedAt: Date;
  deletedByUserId: string;
  deletedByUser: User;
}
