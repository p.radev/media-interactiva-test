﻿import { User } from '../models/user';
import { ISoftDeletable } from './iSoftDeletable';

export interface IObjectWithTimestamps extends ISoftDeletable {
  createdAt: Date;
  createdByUserId: string;
  createdByUser: User;

  updateddAt: Date;
  updatedByUserId: string;
  updatedByUser: User;
}
