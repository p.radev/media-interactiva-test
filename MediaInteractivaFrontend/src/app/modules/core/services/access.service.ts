import { Injectable } from '@angular/core';
import { SessionService } from './session.service';

@Injectable()
export class AccessService {
  constructor(private sessionService: SessionService) {
  }

  public get IsAuthorized(): boolean {
    return !!this.sessionService.CurrentUser;
  }
}
