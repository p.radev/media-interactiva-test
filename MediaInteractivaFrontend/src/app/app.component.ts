import { Component } from '@angular/core';
import { NgProgress } from 'ngx-progressbar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(private ngProgress: NgProgress) {
    const progressRef = ngProgress.ref('globalLoader');
    progressRef.start();
  }
}
