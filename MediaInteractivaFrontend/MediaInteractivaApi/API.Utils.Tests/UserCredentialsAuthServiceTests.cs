using DB.Data;
using DB.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Threading.Tasks;
using API.Utils.Authentication;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Utils.Exceptions;
using Utils.Hashing;

namespace API.Utils.Tests
{
    public class UserCredentialsAuthServiceTests
    {
        private RepositoryProvider _repProvider;

        private UserCredentialsAuthService _authService;

        private const string _testEmail = "main@mailinator.com";
        private const string _testPassword = "testtest";

        private User user = new User {
            FirstName = "Test1",
            LastName = "Unit",
            Email = _testEmail,
            HashedPassword = CryptoHelper.HashPassword(_testPassword),
            IsGlobalAdmin = true
        };


        [SetUp]
        public async Task Setup()
        {
            var options = new DbContextOptionsBuilder<MediaInteractivaDbContext>()
               .UseInMemoryDatabase(databaseName: "AuthServiceDatabase")
               .Options;

            var dbContext = new MediaInteractivaDbContext(options);

            _repProvider = new RepositoryProvider(dbContext, null);

            var myConfiguration = new Dictionary<string, string>
            {
                {"JwtOptions:Issuer", "Test"},
                {"JwtOptions:Audience", "any"},
                {"JwtOptions:Key", "Test85680985478698546"},
                {"JwtOptions:LifeTime", "10000"}
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(myConfiguration)
                .Build();

            _authService = new UserCredentialsAuthService(configuration);

            await _repProvider.Users.AddOrUpdateByIdAsync(user.Id, user);
            await _repProvider.SaveChangesAsync();
        }

        [Test]
        public async Task AuthenticateUserAsyncShouldReturnTokenIfCorrectCredentials()
        {
            var signinResult = await _authService.AuthenticateUserAsync(_repProvider.Users,
                _testEmail,
                _testPassword);

            Assert.IsNotEmpty(signinResult.Token);
        }

        [Test]
        public void AuthenticateUserAsyncShouldThrowErrorIfWrongPass()
        {
            Assert.ThrowsAsync<MediaInteractivaException>(() => _authService.AuthenticateUserAsync(_repProvider.Users, _testEmail, "878797345345"), "Incorrect password");
        }

        [Test]
        public void AuthenticateUserAsyncShouldThrowErrorIfWrongEmail()
        {
            Assert.ThrowsAsync<MediaInteractivaException>(() => _authService.AuthenticateUserAsync(_repProvider.Users, "65567567", _testPassword), "User not found");
        }
    }
}
