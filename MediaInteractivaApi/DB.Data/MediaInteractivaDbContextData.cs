﻿using Db.Data.Repositories;
using DB.Models;
using Microsoft.EntityFrameworkCore;

namespace DB.Data
{
    public partial class MediaInteractivaDbContext
    {
        private void DefineData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AnymalType>().HasData(
                new AnymalType { Id = RepAnymalTypes.DogId, Name = "Dog" },
                new AnymalType { Id = RepAnymalTypes.CatId, Name = "Cat" },
                new AnymalType { Id = RepAnymalTypes.FishId, Name = "Fish" },
                new AnymalType { Id = RepAnymalTypes.BirdId, Name = "Bird" });

            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = RepUsers.AdminId,
                    FirstName = "Root",
                    LastName = "Admin",
                    Email = "main@mailinator.com",
                    HashedPassword = "AQAAAAEAACcQAAAAEAJKeZ4fD8jinehtYPskwJ6Wuau1g1T3e7xyt+3uUyiASHipi9JY0X5hcy+i7RWmZg==",
                    IsGlobalAdmin = true
                });
        }
    }
}