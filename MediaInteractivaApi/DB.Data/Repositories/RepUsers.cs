﻿using System;
using DB.Data;
using DB.Models;

namespace Db.Data.Repositories
{
    public class RepUsers : BaseSoftDeletableRep<User>
    {
        public static readonly Guid AdminId = new Guid("914a2250-fe53-4a65-95d3-8edc6e079884");
        
        public RepUsers(IRepositoryProvider repProvider) : base(repProvider)
        {
        }
    }
}