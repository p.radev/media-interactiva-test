﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
 using DB.Data;
using DB.Data.Queries;
using DB.Models.Classes;

namespace Db.Data.Repositories
{
    public class BaseSoftDeletableRep<T> : BaseRep<T>
        where T : class, ISoftDeletable, IIdentifiable
    {
        public BaseSoftDeletableRep(IRepositoryProvider repProvider) : base(repProvider)
        {
        }
        
        protected override async Task<T> FindByIdIgnoringFilters(Guid id)
        {
            return await Queryable.WithTrashed().FindByIdAsync(id);
        }

        public override void RemoveRange(ICollection<T> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }
        
        public void RemoveRangeFull(ICollection<T> entities)
        {
            base.RemoveRange(entities);
        }
        
        public override T Delete(T entity)
        {
            entity.DeletedAt = DateTime.Now;
            entity.DeletedByUserId = RepositoryProvider.CurrentUserId;

            return entity;
        }

        public async Task<T> DeleteFullByIdAsync(Guid id)
        {
            return await base.DeleteByIdAsync(id);
        }
        
        public void DeleteFull(T entity)
        {
            base.Delete(entity);
        }

        public void Restore(T entity)
        {
            if (entity.DeletedAt is null)
            {
                throw new ApplicationException("Entity not removed yet. Nothing to restore");
            }

            entity.DeletedAt = null;
            entity.DeletedByUserId = null;
        }
    }
}
