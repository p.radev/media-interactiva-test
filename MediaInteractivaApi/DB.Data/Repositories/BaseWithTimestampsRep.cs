﻿using System;
using System.Threading.Tasks;
using DB.Data;
using DB.Models.Classes;

namespace Db.Data.Repositories
{
    public class BaseWithTimestampsRep<T> : BaseSoftDeletableRep<T>
        where T : class, IObjectWithTimestamps, ISoftDeletable, IIdentifiable
    {
        public BaseWithTimestampsRep(IRepositoryProvider repProvider) : base(repProvider)
        {
        }

        public override void Update(T entity)
        {
            entity.UpdatedAt = DateTime.UtcNow;
            entity.UpdatedByUserId = RepositoryProvider.CurrentUserId;
            base.Update(entity);
        }
        
        public override async Task AddAsync(T entity)
        {
            entity.CreatedAt = DateTime.UtcNow;
            entity.CreatedByUserId = RepositoryProvider.CurrentUserId;
            await base.AddAsync(entity);
        }
    }
}
