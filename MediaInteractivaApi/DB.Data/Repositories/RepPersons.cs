﻿using System;
using DB.Data;
using DB.Models;

namespace Db.Data.Repositories
{
    public class RepPersons : BaseSoftDeletableRep<Person>
    {
        
        public RepPersons(IRepositoryProvider repProvider) : base(repProvider)
        {
        }
    }
}