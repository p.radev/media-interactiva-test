﻿using System;
using DB.Data;
using AnymalType = DB.Models.AnymalType;

namespace Db.Data.Repositories
{
    public class RepAnymalTypes : BaseRep<AnymalType>
    {
        public static readonly Guid DogId = new Guid("037859e7-8d80-45ab-8f07-c001ed132b29");
        public static readonly Guid CatId = new Guid("c1423882-97f0-496a-bd3d-7d85447794cf");
        public static readonly Guid FishId = new Guid("bf3f2e80-f0a4-49f6-a8d6-6253727b64b4");
        public static readonly Guid BirdId = new Guid("98557989-7ef7-4277-84b2-60ea0bb489f1");
        
        public RepAnymalTypes(IRepositoryProvider repProvider) : base(repProvider)
        {
        }
    }
}