using System;
using System.Collections;
using System.Threading.Tasks;
using Db.Data.Repositories;
using DB.Models;
using Utils;

namespace DB.Data
{
    public interface IRepositoryProvider
    {
        MediaInteractivaDbContext Db { get; }
        Guid? CurrentUserId { get; }

        RepUsers Users { get; }
        RepAnymalTypes AnymalTypes { get; }
        BaseWithTimestampsRep<Pet> Pets { get; }
        RepPersons Persons { get; }

        void SaveChanges();
        Task SaveChangesAsync();
        Task ReloadAsync<T>(T entry);
        void Dispose();
    }

    public class RepositoryProvider : IDisposable, IRepositoryProvider
    {
        public RepositoryProvider(MediaInteractivaDbContext context, 
            ISessionContextService sessionContextService)
        {
            Db = context;
            SessionContextService = sessionContextService;
        }

        private ISessionContextService SessionContextService { get; }

        public MediaInteractivaDbContext Db { get; }

        public Guid? CurrentUserId => SessionContextService?.CurrentUserId;

        private readonly Hashtable _h = new Hashtable();

        private T GetRep<T>() where T : class
        {
            var typename = typeof(T).FullName;
            if (!_h.ContainsKey(typename))
            {
                var rep = (T) Activator.CreateInstance(typeof(T), this);
                _h[typename] = rep;
            }

            return (T) _h[typename];
        }

        public void SaveChanges()
        {
            Db.SaveChanges();
        }
        
        public async Task SaveChangesAsync()
        {
            await Db.SaveChangesAsync();
        }
        
        public RepUsers Users =>
            GetRep<RepUsers>();
        
        public RepAnymalTypes AnymalTypes =>
            GetRep<RepAnymalTypes>();

        public BaseWithTimestampsRep<Pet> Pets =>
            GetRep<BaseWithTimestampsRep<Pet>>();

        public RepPersons Persons =>
            GetRep<RepPersons>();

        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    Db.Dispose();
                }
            }

            this._disposed = true;
        }

        public async Task ReloadAsync<T>(T entry)
        {
            await Db.Entry(entry).ReloadAsync();
        }
        
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}