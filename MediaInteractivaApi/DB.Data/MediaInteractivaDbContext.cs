﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using DB.Models;
using DB.Models.Classes;
using Microsoft.EntityFrameworkCore;
using AnymalType = DB.Models.AnymalType;

namespace DB.Data
{
    public partial class MediaInteractivaDbContext : DbContext
    {
        private const string DeletedAtProperty = "DeletedAt";

        private static readonly MethodInfo PropertyMethod = typeof(EF)
            .GetMethod(nameof(EF.Property), BindingFlags.Static | BindingFlags.Public)
            .MakeGenericMethod(typeof(DateTime?));

        private static LambdaExpression GetIsDeletedRestriction(Type type)
        {
            var parm = Expression.Parameter(type, "it");
            var prop = Expression.Call(PropertyMethod, parm, Expression.Constant(DeletedAtProperty));
            var condition = Expression.MakeBinary(ExpressionType.Equal, prop, Expression.Constant(null));
            var lambda = Expression.Lambda(condition, parm);
            return lambda;
        }

        public MediaInteractivaDbContext(DbContextOptions<MediaInteractivaDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Pet> Pets { get; set; }
        public DbSet<AnymalType> AnymalTypes { get; set; }
        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var entity in modelBuilder.Model.GetEntityTypes())
            {
                if (typeof(ISoftDeletable).IsAssignableFrom(entity.ClrType) == true)
                {
                    modelBuilder
                        .Entity(entity.ClrType)
                        .HasQueryFilter(GetIsDeletedRestriction(entity.ClrType));
                }
            }

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(x => x.Email).IsUnique();

                entity.HasOne(x => x.DeletedByUser)
                    .WithMany()
                    .HasForeignKey(x => x.DeletedByUserId)
                    .OnDelete(DeleteBehavior.NoAction);
            });

            DefineData(modelBuilder);
        }
    }
}