﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DB.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AnymalTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AnymalTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DeletedByUserId = table.Column<Guid>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    HashedPassword = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    IsGlobalAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Users_DeletedByUserId",
                        column: x => x.DeletedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DeletedByUserId = table.Column<Guid>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    IsEmployee = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persons_Users_DeletedByUserId",
                        column: x => x.DeletedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Pets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    DeletedByUserId = table.Column<Guid>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    CreatedByUserId = table.Column<Guid>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true),
                    UpdatedByUserId = table.Column<Guid>(nullable: true),
                    TypeId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    OwnerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pets_Users_CreatedByUserId",
                        column: x => x.CreatedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pets_Users_DeletedByUserId",
                        column: x => x.DeletedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Pets_Persons_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pets_AnymalTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "AnymalTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pets_Users_UpdatedByUserId",
                        column: x => x.UpdatedByUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AnymalTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("037859e7-8d80-45ab-8f07-c001ed132b29"), "Dog" },
                    { new Guid("c1423882-97f0-496a-bd3d-7d85447794cf"), "Cat" },
                    { new Guid("bf3f2e80-f0a4-49f6-a8d6-6253727b64b4"), "Fish" },
                    { new Guid("98557989-7ef7-4277-84b2-60ea0bb489f1"), "Bird" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DeletedAt", "DeletedByUserId", "Email", "FirstName", "HashedPassword", "IsGlobalAdmin", "LastName" },
                values: new object[] { new Guid("914a2250-fe53-4a65-95d3-8edc6e079884"), null, null, "main@mailinator.com", "Root", "AQAAAAEAACcQAAAAEAJKeZ4fD8jinehtYPskwJ6Wuau1g1T3e7xyt+3uUyiASHipi9JY0X5hcy+i7RWmZg==", true, "Admin" });

            migrationBuilder.CreateIndex(
                name: "IX_Persons_DeletedByUserId",
                table: "Persons",
                column: "DeletedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_CreatedByUserId",
                table: "Pets",
                column: "CreatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_DeletedByUserId",
                table: "Pets",
                column: "DeletedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_OwnerId",
                table: "Pets",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_TypeId",
                table: "Pets",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Pets_UpdatedByUserId",
                table: "Pets",
                column: "UpdatedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_DeletedByUserId",
                table: "Users",
                column: "DeletedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Email",
                table: "Users",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pets");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "AnymalTypes");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
