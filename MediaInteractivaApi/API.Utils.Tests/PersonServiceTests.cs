﻿using API.Utils.Models;
using API.Utils.Services;
using DB.Data;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using DB.Data.Queries;
using System.Threading.Tasks;

namespace API.Utils.Tests
{
    public class PersonServiceTests
    {
        private RepositoryProvider _repProvider;

        private PersonsService _personService;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<MediaInteractivaDbContext>()
               .UseInMemoryDatabase(databaseName: "PersonServiceDatabase")
               .Options;

            var dbContext = new MediaInteractivaDbContext(options);

            _repProvider = new RepositoryProvider(dbContext, null);

            _personService = new PersonsService(_repProvider);
        }

        [Test]
        public async Task CreatePersonShoudInsertTheEntry()
        {
            var personsCountBeforeExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            await _personService.CreatePerson("Test1", "Unit", true);

            var personsCountAfterExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            Assert.Greater(personsCountAfterExecution, personsCountBeforeExecution);
        }

        [Test]
        public async Task UpdatePersonShouldNotInsertTheEntry()
        {
            var insertedPerson = await _personService.CreatePerson("Test1", "Unit", true);

            var personsCountBeforeExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            var updateModel = new PersonUpdateRequestModel { FirstName = "updated", LastName = "test", IsEmployee = false };

            await _personService.UpdatePerson(insertedPerson.Id, updateModel);

            var personsCountAfterExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            Assert.AreEqual(personsCountBeforeExecution, personsCountAfterExecution);
        }

        [Test]
        public async Task UpdatePersonShouldAffectTheDbData()
        {
            var initialNameValue = "Test1";
            var updatedNameValue = "Test2";

            var insertedPerson = await _personService.CreatePerson(initialNameValue, "Unit", true);


            var updateModel = new PersonUpdateRequestModel { FirstName = updatedNameValue, LastName = "test", IsEmployee = false };
            await _personService.UpdatePerson(insertedPerson.Id, updateModel);


            var entryFromDb = await _repProvider.Persons.Queryable.GetByIdAsync(insertedPerson.Id);

            Assert.AreNotEqual(entryFromDb.FirstName, initialNameValue);
            Assert.AreEqual(entryFromDb.FirstName, updatedNameValue);
        }

        [Test]
        public async Task UpdatePersonShoudThrowExceptionIfIncorrectId()
        {
            var personsCountBeforeExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            Assert.ThrowsAsync<ApplicationException>(async () => await _personService.UpdatePerson(Guid.Empty, new PersonUpdateRequestModel()));

            var personsCountAfterExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            Assert.AreEqual(personsCountBeforeExecution, personsCountAfterExecution);
        }

        [Test]
        public async Task FullyRemovePersonShouldRemoveThePersonFromStorage()
        {
            var person = await _personService.CreatePerson("Test1", "Unit", true);

            await _personService.FullyRemovePerson(person.Id);
            var result = await _repProvider.Persons.Queryable.FindByIdAsync(person.Id);

            Assert.IsNull(result);
        }
    }
}
