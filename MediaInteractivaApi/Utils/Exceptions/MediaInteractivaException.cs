﻿using System;
using System.Collections.Generic;

namespace Utils.Exceptions
{
    [Serializable]
    public class MediaInteractivaException : Exception
    {
        public Dictionary<string, string> FieldSpecificMessages { get; } = new Dictionary<string, string>();
        public string OverallMessage { get; }
        
        public MediaInteractivaException()
        {
        }

        public MediaInteractivaException(string message, Dictionary<string, string> fieldSpecificMessages = null) : base(message)
        {
            OverallMessage = message;
            
            if (fieldSpecificMessages != null)
            {
                FieldSpecificMessages = fieldSpecificMessages;
            }
        }
    }
}