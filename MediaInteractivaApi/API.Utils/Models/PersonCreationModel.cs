﻿namespace API.Utils.Models
{
    public class PersonCreationModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsEmployee { get; set; }
    }
}