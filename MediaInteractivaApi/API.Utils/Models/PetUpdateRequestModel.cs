﻿using System;

namespace API.Utils.Models
{
    public class PetUpdateRequestModel
    {
        public string Name { get; set; }
    }
}