﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Utils.Models.Filters
{
    public class DashboardFilter
    {
        public Guid ClientId { get; set; }
        public Guid? AccountId { get; set; }

        public bool IncludeCredits { get; set; }
        public bool IncludeUpfrontReservation { get; set; }
        public bool IncludeRecurringReservation { get; set; }
        public bool IncludeOneTimeFees { get; set; }
        public bool IncludeSupportCharges { get; set; }
        public bool IncludeOtherCharges { get; set; }
    }
}