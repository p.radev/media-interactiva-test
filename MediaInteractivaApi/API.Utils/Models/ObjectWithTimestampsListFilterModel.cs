﻿using System;
using System.Collections.Generic;

namespace API.Utils.Models
{
    public class ObjectWithTimestampsListFilterModel : SoftDeletableListFilterModel
    {
        public IEnumerable<Guid> CreatedBy { get; set; }
        public DateTime? CreatedFrom { get; set; }
        public DateTime? CreatedTo { get; set; }
        
        public IEnumerable<Guid> UpdatedBy { get; set; }
        public DateTime? UpdatedFrom { get; set; }
        public DateTime? UpdatedTo { get; set; }
    }
}