﻿using System;

namespace API.Utils.Models
{
    public class PetCreationModel
    {
        public string Name { get; set; }
        public Guid TypeId { get; set; }
        public Guid OwnerId { get; set; }
    }
}