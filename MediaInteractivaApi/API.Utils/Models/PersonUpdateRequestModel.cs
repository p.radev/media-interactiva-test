﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Utils.Models
{
    public class PersonUpdateRequestModel
    {
        [Required]
        [MinLength(3)]
        public string FirstName { get; set; }
        [Required]
        [MinLength(3)]
        public string LastName { get; set; }
        [Required]
        public bool IsEmployee { get; set; }
    }
}