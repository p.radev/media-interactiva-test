﻿using System;

namespace API.Utils.Models
{
    public class PetsFilterModel: ObjectWithTimestampsListFilterModel
    {
        public Guid? OwnerId { get; set; }
    }
}
