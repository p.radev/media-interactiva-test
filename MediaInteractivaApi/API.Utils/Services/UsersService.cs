﻿﻿using System;
 using System.Collections.Generic;
 using System.Threading.Tasks;
 using API.Utils.Models;
 using DB.Data;
 using DB.Data.Queries;
 using DB.Models;
 using Microsoft.EntityFrameworkCore;
 using Utils;
 using Utils.Exceptions;
 using Utils.Hashing;

namespace API.Utils.Services
{
    public interface IUsersService
    {
        Task<bool> GetIsEmailAlreadyTaken(string email);
        Task<User> CreateUser(string email, string firstName, string lastName, string password);
    }

    public class UsersService : IUsersService
    {
        private IRepositoryProvider RepositoryProvider { get; }
        private ISessionContextService SessionContextService { get; }

        public UsersService(IRepositoryProvider repositoryProvider, ISessionContextService sessionContextService)
        {
            RepositoryProvider = repositoryProvider;
            SessionContextService = sessionContextService;
        }
        
        
        public async Task<bool> GetIsEmailAlreadyTaken(string email)
        {
            return await RepositoryProvider.Users
                .Queryable
                .AnyAsync(x => x.Email.ToLower().Equals(email));
        }

        public async Task<User> CreateUser(string email, string firstName, string lastName, string password)
        {
            if (await GetIsEmailAlreadyTaken(email))
            {
                throw new BadRequestException("Email is already taken");
            }
            
            var hashedPassword = CryptoHelper.HashPassword(password);
            var newUser = new User
            {
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                HashedPassword = hashedPassword
            };

            await RepositoryProvider.Users.AddAsync(newUser);
            await RepositoryProvider.SaveChangesAsync();

            return newUser;
        }
    }
}