﻿﻿using System;
 using System.Threading.Tasks;
 using API.Utils.Models;
 using DB.Data;
 using DB.Data.Queries;
 using DB.Models;

namespace API.Utils.Services
{
    public interface IPetsService
    {
        Task<Pet> GetPet(Guid id);
        Task<Pet> CreatePet(string name, Guid typeId, Guid ownerId);
        Task<Pet> UpdatePet(Guid id, PetUpdateRequestModel model);
        Task RemovePet(Guid id);
    }

    public class PetsService : IPetsService
    {
        private IRepositoryProvider RepositoryProvider { get; }

        public PetsService(IRepositoryProvider repositoryProvider)
        {
            RepositoryProvider = repositoryProvider;
        }
        
        
        public async Task<Pet> GetPet(Guid id)
        {
            return await RepositoryProvider.Pets.Queryable.GetByIdAsync(id);
        }

        public async Task<Pet> CreatePet(string name, Guid typeId, Guid ownerId)
        {

            var type = await RepositoryProvider.AnymalTypes.Queryable.GetByIdAsync(typeId);

            var owner = await RepositoryProvider.Persons.Queryable.GetByIdAsync(ownerId);

            var newPet = new Pet
            {
                Name = name,
                Type = type,
                Owner = owner
            };

            await RepositoryProvider.Pets.AddAsync(newPet);
            await RepositoryProvider.SaveChangesAsync();

            return newPet;
        }
        
        public async Task<Pet> UpdatePet(Guid id, PetUpdateRequestModel model)
        {
            var modelToUpdate = await RepositoryProvider.Pets.Queryable.GetByIdAsync(id);

            modelToUpdate.Name = model.Name;

            RepositoryProvider.Pets.Update(modelToUpdate);
            await RepositoryProvider.SaveChangesAsync();

            return modelToUpdate;
        }
       
        public async Task RemovePet(Guid id)
        {

            var entityToRemove = await RepositoryProvider.Pets
                .Queryable
                .WithTrashed()
                .GetByIdAsync(id);
            RepositoryProvider.Pets.DeleteFull(entityToRemove);

            await RepositoryProvider.SaveChangesAsync();
        }

    }
}