﻿﻿using System;
 using System.Threading.Tasks;
 using API.Utils.Models;
 using DB.Data;
 using DB.Data.Queries;
 using DB.Models;

namespace API.Utils.Services
{
    public interface IPersonsService
    {
        Task<Person> GetPerson(Guid id);
        Task<Person> CreatePerson(string firstName, string lastName, bool isEmployee);
        Task<Person> UpdatePerson(Guid id, PersonUpdateRequestModel model);
        Task DeactivatePerson(Guid id);
        Task FullyRemovePerson(Guid id);
        Task<Person> RestorePerson(Guid id);
    }

    public class PersonsService : IPersonsService
    {
        private IRepositoryProvider RepositoryProvider { get; }

        public PersonsService(IRepositoryProvider repositoryProvider)
        {
            RepositoryProvider = repositoryProvider;
        }
        
        
        public async Task<Person> GetPerson(Guid id)
        {
            return await RepositoryProvider.Persons.Queryable.GetByIdAsync(id);
        }

        public async Task<Person> CreatePerson(string firstName, string lastName, bool isEmployee)
        {
            
            var newPerson = new Person
            {
                FirstName = firstName,
                LastName = lastName,
                IsEmployee = isEmployee
            };

            await RepositoryProvider.Persons.AddAsync(newPerson);
            await RepositoryProvider.SaveChangesAsync();

            return newPerson;
        }
        
        public async Task<Person> UpdatePerson(Guid id, PersonUpdateRequestModel model)
        {
            var modelToUpdate = await RepositoryProvider.Persons.Queryable.GetByIdAsync(id);

            modelToUpdate.FirstName = model.FirstName;
            modelToUpdate.LastName = model.LastName;
            modelToUpdate.IsEmployee = model.IsEmployee;

            RepositoryProvider.Persons.Update(modelToUpdate);
            await RepositoryProvider.SaveChangesAsync();

            return modelToUpdate;
        }

        public async Task DeactivatePerson(Guid id)
        {
            var entityToDeactivate = await RepositoryProvider.Persons.Queryable.GetByIdAsync(id);
            RepositoryProvider.Persons.Delete(entityToDeactivate);

            await RepositoryProvider.SaveChangesAsync();
        }

        public async Task FullyRemovePerson(Guid id)
        {

            var entityToRemove = await RepositoryProvider.Persons
                .Queryable
                .WithTrashed()
                .GetByIdAsync(id);
            RepositoryProvider.Persons.DeleteFull(entityToRemove);

            await RepositoryProvider.SaveChangesAsync();
        }

        public async Task<Person> RestorePerson(Guid id)
        {

            var clientToRestore = await RepositoryProvider.Persons
                .Queryable
                .WithTrashed()
                .GetByIdAsync(id);
            RepositoryProvider.Persons.Restore(clientToRestore);

            await RepositoryProvider.SaveChangesAsync();

            return clientToRestore;
        }

    }
}