﻿using System.Linq;
using System.Linq.Dynamic.Core;
using API.Utils.Models;
using DB.Data.Queries;
using DB.Models;
using DB.Models.Classes;

namespace API.Utils.Extensions
{
    public static class FilteringExtensions
    {
        public static IQueryable<T> ApplyBaseFilter<T>(
            this IQueryable<T> query,
            BaseFilterModel filters) where T : class
        {
            return query.IncludePropertiesFromString(filters.IncludeProperties);
        }

        public static IQueryable<T> ApplySoftDeletableFilter<T>(
            this IQueryable<T> query,
            SoftDeletableListFilterModel filters) where T : class, ISoftDeletable
        {
            query = query.ApplyBaseFilter(filters);

            query = query.WithTrashed(filters.WithTrashed);

            if (filters.DeletedBy != null && filters.DeletedBy.Any())
            {
                query = query.Where(x => x.DeletedByUserId != null
                                         && filters.DeletedBy.Any(y => y == x.DeletedByUserId));
            }

            if (filters.DeletedFrom != null)
            {
                query = query.Where(x => x.DeletedAt >= filters.DeletedFrom);
            }

            if (filters.DeletedTo != null)
            {
                query = query.Where(x => x.DeletedAt <= filters.DeletedTo);
            }

            return query;
        }

        public static IQueryable<T> ApplyObjWithTimestampsFilter<T>(
            this IQueryable<T> query,
            ObjectWithTimestampsListFilterModel filters) where T : class, IObjectWithTimestamps
        {
            query = query.ApplySoftDeletableFilter(filters);

            if (filters.CreatedBy != null && filters.CreatedBy.Any())
            {
                query = query.Where(x => x.CreatedByUserId != null
                                         && filters.CreatedBy.Any(y => y == x.CreatedByUserId));
            }

            if (filters.CreatedFrom != null)
            {
                query = query.Where(x => x.CreatedAt >= filters.CreatedFrom);
            }

            if (filters.CreatedTo != null)
            {
                query = query.Where(x => x.CreatedAt <= filters.CreatedTo);
            }

            if (filters.UpdatedBy != null && filters.UpdatedBy.Any())
            {
                query = query.Where(x => x.UpdatedByUserId != null
                                         && filters.UpdatedBy.Any(y => y == x.UpdatedByUserId));
            }

            if (filters.UpdatedFrom != null)
            {
                query = query.Where(x => x.UpdatedAt >= filters.UpdatedFrom);
            }

            if (filters.UpdatedTo != null)
            {
                query = query.Where(x => x.UpdatedAt <= filters.UpdatedTo);
            }

            return query;
        }

        public static IQueryable<Pet> ApplyPetsFilter(this IQueryable<Pet> query, PetsFilterModel filters)
        {
            query = query.ApplyObjWithTimestampsFilter(filters);


            if (filters.OwnerId != null)
            {
                query = query.Where(x => x.OwnerId == filters.OwnerId);
            }

            return query;
        }
    }
}
