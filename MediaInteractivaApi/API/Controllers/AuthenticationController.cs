﻿using System;
using System.Threading.Tasks;
using API.Classes;
using API.Models;
using API.Utils.Authentication;
using API.Utils.Models.ResponseModels;
using DB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("api/auth")]
    public class AuthenticationController : BaseApiController
    {
        protected IUserCredentialsAuthService CredentialsAuthService { get; }

        public AuthenticationController(IServiceProvider serviceProvider,
            IUserCredentialsAuthService credentialsAuthService) : base(serviceProvider)
        {
            CredentialsAuthService = credentialsAuthService;
        }

        /// <summary>
        /// Signin using login+pass authentication schema
        /// </summary>
        /// <param name="loginModel">Signin credentials</param>
        /// <response code="200">User model with JWT token</response>
        [HttpPost("signin-with-credentials")]
        public async Task<ActionResult<ApiResponse<AuthenticationResult>>> Signin(LoginWithCredentialsRequest loginModel)
        {
            var signinResult = await CredentialsAuthService.AuthenticateUserAsync(RepProvider.Users,
                loginModel.Login,
                loginModel.Password);

            return SuccessResponse(signinResult);
        }

        /// <summary>
        /// Returns current user if valid token provided
        /// </summary>
        /// <response code="200">Current user model</response>
        /// <response code="401">Token invalid</response>
        [HttpGet("current-user")]
        [Authorize]
        public async Task<ActionResult<ApiResponse<User>>> GetCurrentUser()
        {
            var currentUser = await GetCurrentUserAsync();
            return SuccessResponse(currentUser);
        }
    }
}
