﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Classes;
using API.Utils.Extensions;
using API.Utils.Models;
using API.Utils.Models.ResponseModels;
using API.Utils.Services;
using Db.Data.Repositories;
using DB.Data.Queries;
using DB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route(("api/pets"))]
    [Authorize]
    public class PetsController : BaseApiController
    {
        private IPetsService PetsService { get; }


        public PetsController(IServiceProvider serviceProvider,
            IPetsService petsService) : base(serviceProvider)
        {
            PetsService = petsService;
        }

        /// <summary>
        /// Create new pet
        /// </summary>
        [HttpPost("create")]
        public async Task<ActionResult<ApiResponse<Pet>>> CreatePet(PetCreationModel model)
        {
            var newClient = await PetsService.CreatePet(model.Name, model.TypeId, model.OwnerId);

            return SuccessCreateResponse(newClient);
        }

        /// <summary>
        /// Get the list of all the persons
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<ApiResponse<ListWithTotals<Pet>>>> GetAllPetsList([FromQuery] PetsFilterModel filters)
        {
            var results = await RepProvider.Pets
                .Queryable
                .Include(x => x.Owner)
                .Include(x => x.Type)
                .ApplyPetsFilter(filters)
                .AsNoTracking()
                .ToListWithTotalsAsync(filters.PageSize,filters.Page, filters.OrderedBy, filters.OrderReversed);

            return SuccessResponse(results);
        }

        /// <summary>
        /// Get the list of all the persons
        /// </summary>
        [HttpGet("types")]
        [AllowAnonymous]
        public async Task<ActionResult<ApiResponse<List<AnymalType>>>> GetTypesList()
        {
            var results = await RepProvider.AnymalTypes
                .Queryable
                .AsNoTracking()
                .ToListAsync();

            return SuccessResponse(results);
        }

        /// <summary>
        /// Get pet by id with list of pets
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<ApiResponse<Pet>>> GetById(Guid id)
        {
            var result = await RepProvider.Pets
                .Queryable
                .WithTrashed()
                .Include(x => x.Owner)
                .Include(x => x.Type)
                .GetByIdAsync(id);

            return SuccessResponse(result);
        }

        /// <summary>
        /// Update general information of pet
        /// </summary>
        [HttpPut("{id}")]
        public async Task<ActionResult<ApiResponse<Pet>>> Update(Guid id, [FromBody] PetUpdateRequestModel model)
        {
            var result = await PetsService.UpdatePet(id, model);

            return SuccessUpdateResponse(result);
        }

        [HttpPost("fully-remove/{id}")]
        public async Task<ActionResult<ApiResponse<bool>>> Remove(Guid id)
        {
            await PetsService.RemovePet(id);

            return SuccessDeleteResponse(true);
        }
    }
}
