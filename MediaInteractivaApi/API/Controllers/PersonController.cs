﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Classes;
using API.Utils.Extensions;
using API.Utils.Models;
using API.Utils.Models.ResponseModels;
using API.Utils.Services;
using DB.Data.Queries;
using DB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    [Route(("api/persons"))]
    [Authorize]
    public class PersonController : BaseApiController
    {
        private IPersonsService PersonsService { get; }


        public PersonController(IServiceProvider serviceProvider,
            IPersonsService personsService) : base(serviceProvider)
        {
            PersonsService = personsService;
        }

        /// <summary>
        /// Create new person
        /// </summary>
        [HttpPost("create")]
        public async Task<ActionResult<ApiResponse<Person>>> CreatePerson(PersonCreationModel model)
        {
            var newClient = await PersonsService.CreatePerson(model.FirstName, model.LastName, model.IsEmployee);

            return SuccessCreateResponse(newClient);
        }

        /// <summary>
        /// Get the list of all the persons
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<ApiResponse<ListWithTotals<Person>>>> GetPersonsList([FromQuery] SoftDeletableListFilterModel filters)
        {
            var results = await RepProvider.Persons
                .Queryable
                .Include(x => x.Pets)
                .ApplySoftDeletableFilter(filters)
                .AsNoTracking()
                .ToListWithTotalsAsync(filters.PageSize,filters.Page, filters.OrderedBy, filters.OrderReversed);

            return SuccessResponse(results);
        }

        /// <summary>
        /// Get the list of all the persons
        /// </summary>
        [HttpGet("all")]
        public async Task<ActionResult<ApiResponse<List<Person>>>> GetAllPersonsList()
        {
            var results = await RepProvider.Persons
                .Queryable
                .ToListAsync();

            return SuccessResponse(results);
        }

        /// <summary>
        /// Get person by id with list of pets
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<ApiResponse<Person>>> GetById(Guid id)
        {
            var result = await RepProvider.Persons
                .Queryable
                .WithTrashed()
                .Include(x => x.Pets)
                .GetByIdAsync(id);

            return SuccessResponse(result);
        }

        /// <summary>
        /// Update general information of person
        /// </summary>
        [HttpPut("{id}")]
        public async Task<ActionResult<ApiResponse<Person>>> Update(Guid id, [FromBody] PersonUpdateRequestModel model)
        {
            var result = await PersonsService.UpdatePerson(id, model);

            return SuccessUpdateResponse(result);
        }

        [HttpPost("deactivate/{id}")]
        public async Task<ActionResult<ApiResponse<bool>>> Deactivate(Guid id)
        {
            await PersonsService.DeactivatePerson(id);

            return SuccessDeleteResponse(true);
        }

        [HttpPost("fully-remove/{id}")]
        public async Task<ActionResult<ApiResponse<bool>>> FullyRemove(Guid id)
        {
            await PersonsService.FullyRemovePerson(id);

            return SuccessDeleteResponse(true);
        }

        [HttpPost("restore/{id}")]
        public async Task<ActionResult<ApiResponse<Person>>> Restore(Guid id)
        {
            var entity = await PersonsService.RestorePerson(id);

            return SuccessUpdateResponse(entity);
        }
    }
}
