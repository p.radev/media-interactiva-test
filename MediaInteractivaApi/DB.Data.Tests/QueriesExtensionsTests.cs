﻿using Db.Data.Repositories;
using DB.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using DB.Data.Queries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DB.Data.Tests
{
    public class QueriesExtensionsTests
    {
        private BaseRep<Person> _baseRep;

        private RepositoryProvider _repProvider;
        private readonly Guid personId = new Guid("914a2250-fe53-4a65-95d3-8edc6e079883");

        [SetUp]
        public async Task Setup()
        {
            var options = new DbContextOptionsBuilder<MediaInteractivaDbContext>()
            .UseInMemoryDatabase(databaseName: "QuerisExtendionsDatabase")
            .Options;

            var dbContext = new MediaInteractivaDbContext(options);

            _repProvider = new RepositoryProvider(dbContext, null);

            _baseRep = new BaseRep<Person>(_repProvider);
        }

        [Test]
        public async Task GetByIdAsyncShouldReturnTheNeededEntry()
        {
            await _baseRep.AddOrUpdateRangeAsync(GetFakeListOfPersons());
            await _repProvider.SaveChangesAsync();

            var person = await _baseRep.Queryable
                .WithTrashed()
                .GetByIdAsync(personId);

            Assert.AreEqual(personId, person.Id);
        }

        [Test]
        public void GetByIdAsyncShouldThrowErrorIfEntryWithIdIsNotPresented()
        {
            Assert.ThrowsAsync<ApplicationException>(() => _baseRep.Queryable.WithTrashed().GetByIdAsync(Guid.Empty));
        }

        [Test]
        public async Task ToListWithTotalsAsyncShouldReturnCorrectTotalNumber()
        {
            var listWithTotals = await _baseRep.Queryable
                .WithTrashed()
                .ToListWithTotalsAsync();

            var entriesInTableCount = await _baseRep.Queryable.WithTrashed().CountAsync();

            Assert.AreEqual(entriesInTableCount, listWithTotals.TotalCount);
        }

        private IEnumerable<Person> GetFakeListOfPersons()
        {
            var persons = new List<Person>
            {
                new Person { Id = personId, FirstName = "Test1", LastName = "Unit", IsEmployee = true },
                new Person { FirstName = "Test2", LastName = "Unit", IsEmployee = true },
                new Person { FirstName = "Test3", LastName = "Unit", IsEmployee = false }
            };

            return persons;
        }
    }
}
