﻿using Db.Data.Repositories;
using DB.Models;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DB.Data.Tests
{
    public class BaseRepTests
    {
        private BaseRep<Person> _baseRep;

        private RepositoryProvider _repProvider;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<MediaInteractivaDbContext>()
                .UseInMemoryDatabase(databaseName: "MediaInteractivaDatabase")
                .Options;

            var dbContext = new MediaInteractivaDbContext(options);

            _repProvider = new RepositoryProvider(dbContext, null);

            _baseRep = new BaseRep<Person>(_repProvider);
        }

        private ICollection<Person> GetFakeListOfPersons()
        {
            var persons = new List<Person>
            {
                new Person { FirstName = "Test1", LastName = "Unit", IsEmployee = true },
                new Person { FirstName = "Test2", LastName = "Unit", IsEmployee = true },
                new Person { FirstName = "Test3", LastName = "Unit", IsEmployee = false }
            };

            return persons;
        }

        [Test]
        public async Task AddOrUpdateRangeAsyncShouldInsertEntries()
        {
            var personsToInsert = GetFakeListOfPersons();

            await _baseRep.AddOrUpdateRangeAsync(personsToInsert);
            await _repProvider.SaveChangesAsync();

            var personsCount = await _baseRep.GetAllItemsCountAsync();

            Assert.AreEqual(personsToInsert.Count, personsCount);
        }

        [Test]
        public async Task UpdateShouldChangeTheStorageData()
        {
            var personToUpdate = await _baseRep.Queryable.FirstOrDefaultAsync();

            personToUpdate.FirstName = "TestUpdate";
            _baseRep.Update(personToUpdate);

            await _repProvider.SaveChangesAsync();

            var updatedPerson = await _baseRep.Queryable.FirstOrDefaultAsync(x => x.Id == personToUpdate.Id);

            Assert.AreEqual(personToUpdate.FirstName, updatedPerson.FirstName);
        }

        [Test]
        public async Task DeleteByIdAsyncShouldDecreaseTheTableEntriesCount()
        {
            var personToRemove = await _baseRep.Queryable.FirstOrDefaultAsync();

            var personsCountBeforeExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            await _baseRep.DeleteByIdAsync(personToRemove.Id);

            await _repProvider.SaveChangesAsync();

            var personsCountAfterExecution = await _repProvider.Persons.GetAllItemsCountAsync();

            Assert.AreNotEqual(personsCountBeforeExecution, personsCountAfterExecution);
        }
    }
}
