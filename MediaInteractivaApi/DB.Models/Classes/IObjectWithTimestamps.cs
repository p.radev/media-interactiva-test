﻿using System;

namespace DB.Models.Classes
{
    public interface IObjectWithTimestamps : ISoftDeletable
    {
        DateTime? CreatedAt { get; set; }
        Guid? CreatedByUserId { get; set; }
        DateTime? UpdatedAt { get; set; }
        Guid? UpdatedByUserId { get; set; }
    }
}