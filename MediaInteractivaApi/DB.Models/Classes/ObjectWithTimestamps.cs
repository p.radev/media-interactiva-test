﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace DB.Models.Classes
{
    public class ObjectWithTimestamps : SoftDeletable, IObjectWithTimestamps
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime? CreatedAt { get; set; }
        public Guid? CreatedByUserId { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public Guid? UpdatedByUserId { get; set; }


        [ForeignKey("CreatedByUserId")]
        public User CreatedByUser { get; set; }
        [ForeignKey("UpdatedByUserId")]
        public User UpdatedByUser { get; set; }

        public static readonly string[] ProtectedProps = typeof(IObjectWithTimestamps)
            .GetProperties()
            .Select(prop => prop.Name)
            .ToArray();
    }
}