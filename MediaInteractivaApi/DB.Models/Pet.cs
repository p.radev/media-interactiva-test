﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DB.Models.Classes;

namespace DB.Models
{
    [Table("Pets")]
    public class Pet : ObjectWithTimestamps, IIdentifiable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid TypeId { get; set; }

        public string Name { get; set; }

        public Guid OwnerId { get; set; }

        [ForeignKey("OwnerId")]
        public Person Owner { get; set; }

        [ForeignKey("TypeId")]
        public AnymalType Type { get; set; }
    }
}
