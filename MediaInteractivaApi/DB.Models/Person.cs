﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DB.Models.Classes;
using Newtonsoft.Json;

namespace DB.Models
{
    [Table("Persons")]
    public class Person : SoftDeletable, IIdentifiable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsEmployee { get; set; }

        public ICollection<Pet> Pets { get; set; }

        public string FullName => string.Concat(FirstName, " ", LastName);
    }
}